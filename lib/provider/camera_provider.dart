import 'package:camera_app/screen/upload_video.dart';

import 'package:flutter/material.dart';

class CameraProvider extends ChangeNotifier {
  List cameralist = [];

  void addCamera(dynamic path, BuildContext context) {
    cameralist.add(path);
    notifyListeners();
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => UploadVideo()));
  }
}
