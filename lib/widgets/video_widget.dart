import 'dart:io';

import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';


class Videowidget extends StatefulWidget {
  final video;

  Videowidget({
    this.video,
  });

  @override
  _VideowidgetState createState() => _VideowidgetState();
}

class _VideowidgetState extends State<Videowidget> {
  bool isLoadingProgress = false;
 

  VideoPlayerController? videoPlayerController;

  ChewieController? _chewieController;

  @override
  void dispose() {
    videoPlayerController!.dispose();

    _chewieController?.dispose();
    super.dispose();
  }

  Future<void> initializePlayer() async {
    videoPlayerController = VideoPlayerController.file(
       File(widget.video )
      //  "REC5353948425624490713.mp4",
    );

    await Future.wait([
      videoPlayerController!.initialize(),
    ]);
    _createChewieController();
    setState(() {});
  }

  void _createChewieController() {
    _chewieController = ChewieController(
      videoPlayerController: videoPlayerController!,
      autoPlay: true,
      looping: true,
    );
  }

  @override
  void initState() {
    initializePlayer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.transparent,
        elevation: 0,
//  leading: IconButton(
//    color: Colors.white,
//             onPressed: () async {
//               var storageService, data;
//               storageService = locator<StorageService>();
//               data = await storageService.getData("route");
//               // videoControllerWrapper.

//               data == '/video-description-screen'
//                   ? Navigator.pushReplacement(
//                       context,
//                       MaterialPageRoute(
//                         builder: (context) => VideoDescription(
//                             videoDescription: widget.video,
//                             videoList: widget.videoList,
//                             consultant: widget.consultant),
//                       ))
//                   : Navigator.pushReplacement(
//                       context,
//                       MaterialPageRoute(
//                         builder: (context) => DayWisePlanScreen(
//                           widget.videoList,
//                           widget.consultant,
//                         ),
//                       ));
//               // videoControllerWrapper.dispose();
//             },
//             icon: Icon(Icons.arrow_back),
//           ),
      ),
      body: Center(
        child: _chewieController != null &&
                _chewieController!.videoPlayerController.value.isInitialized
            ? Chewie(
                controller: _chewieController!,
              )
            : Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  CircularProgressIndicator(),
                  SizedBox(height: 50),
                  Text('Loading'),
                ],
              ),
      ),
    );
  }
}
