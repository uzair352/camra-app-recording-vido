import 'dart:io';

import 'package:camera_app/provider/camera_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_camera/flutter_camera.dart';
import 'package:provider/provider.dart';

class CameraPage extends StatefulWidget {
  
   

  @override
  _CameraPageState createState() => _CameraPageState();
}

class _CameraPageState extends State<CameraPage> {
  @override
  Widget build(BuildContext context) {
   
    return FlutterCamera(
      color: Colors.amber,
      // onImageCaptured: (value) {
        
      //   final path = value.path;
     
      //   if (path!.contains('.jpg')) {
      //     showDialog(
      //         context: context,
      //         builder: (context) {
      //           return AlertDialog(
      //             content: Image.file(File(path!)),
      //           );
      //         });
      //   }
      // },
       onVideoRecorded: (value) {
         Provider.of<CameraProvider>(context,listen: false).addCamera(value.path,context);

        final path = value.path;
        print(path);
        
      },
    );
    // return Container();
  }
}