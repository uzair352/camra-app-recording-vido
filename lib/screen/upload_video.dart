import 'package:camera_app/provider/camera_provider.dart';
import 'package:camera_app/widgets/video_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class UploadVideo extends StatefulWidget {
  const UploadVideo({Key? key}) : super(key: key);

  @override
  State<UploadVideo> createState() => _UploadVideoState();
}

class _UploadVideoState extends State<UploadVideo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Upload Video Gallery"),
      ),
      body: Container(
        child: Padding(
          padding: const EdgeInsets.all(28.0),
          child: Column(
            children: [

             Consumer<CameraProvider>(
               builder: (context,p,_){
                 return   GridView.builder(
                   shrinkWrap: true,
                             gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 200,
                    childAspectRatio: 3 / 3,
                    crossAxisSpacing: 20,
                    mainAxisSpacing: 20),
                             itemCount: p.cameralist.length,
                             itemBuilder: (BuildContext ctx, index) {
                  return Container(
                    
                   
                    child:Videowidget(video: p.cameralist[index],),
                    
                    decoration: BoxDecoration(
                        color: Colors.amber,
                        borderRadius: BorderRadius.circular(15)),
                  );
                             });
               },
              
             ),
            ],
          ),
        ),
      ),
    );
  }
}
