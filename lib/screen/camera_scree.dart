import 'package:camera_app/screen/camera_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CameraSceen extends StatelessWidget {
  const CameraSceen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton:
          FloatingActionButton.extended(onPressed: () {
           Navigator.push(context,  CupertinoPageRoute(builder: (context)=>  CameraPage()));
          }, label: Text("Start")),
      body: Container(
        child: Column(
          children: [
            Center(
              child: Text("Flutter Camera Action"),
            )
          ],
        ),
      ),
    );
  }
}
